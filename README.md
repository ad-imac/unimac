# unimac

### Pitch 

Aujourd’hui, les IMAC sont reliés entre eux. Cependant, des informations restent difficiles d’accès : portfolio des étudiants, réseaux sociaux, projets IMAC, parcours professionnel, etc. 
Ces difficultés de communication ne sont qu’exacerbées par la crise sanitaire : les promotions et les étudiants se connaissent peu voire pas.

UNIMAC est le réseau social interne à l’IMAC qui permet aux étudiants de trouver des informations concernant leur camarades de promotions passées : 
- Avatar au choix de l’étudiant (yeux, nez, bouche, cheveux) (ou photos) ;
- Réseaux sociaux (instagram, linkedin, etc.);
- Parcours étudiants (ce qu’ils ont fait avant l’IMAC, spécialisation domaine) ;
- Lien du portfolio (les projets y figurent) ;
- Parcours professionnel (ce qu’ils font actuellement).
