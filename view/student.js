var id_student;

// récupération de l'identifiant de l'étudiant à partir de la session
fetch('../routeur.php/session', {method: 'GET'})
  .then( response => response.json() )
  .then ( data => {
      id_student = data;
      //récupération info étudiant
      fetch(`../routeur.php/student/${id_student}`, { method: 'GET'})
      .then( response => response.json() )
      .then( data => {
        displayStudentInHTML(data);
      })
    .catch(error => {console.log(error) });	
    })    
.catch(error => { console.log(error) });

/**************************STUDENT**************************/

// Affichage des informations de l'étudiant
function displayStudentInHTML(student) {
   
    document.getElementById('firstname').innerHTML = student.firstname_student;
    document.getElementById('lastname').innerHTML = student.lastname_student;
    document.getElementById('field').innerHTML = student.field_student;
    document.getElementById('mail').innerHTML = student.mail_student;
    document.getElementById('city').innerHTML = student.city_student;
    document.getElementById('year').innerHTML += student.year_student;
    document.getElementById('portfolio').innerHTML = student.portfolio_student;
    document.getElementById('portfolio').href = student.portfolio_student;
    const socialMedia_div = document.getElementById('social-media');
    if (student.linkedin_student)
        socialMedia_div.innerHTML += `<a href="${student.linkedin_student}"><img src="img/linkedin.png" width="30px"></a>`;
    if (student.insta_student)
        socialMedia_div.innerHTML += `<a href="${student.insta_student}"><img src="img/instagram.png" width="30px"></a>`;
    if (student.facebook_student)
        socialMedia_div.innerHTML += `<a href="${student.facebook_student}"><img src="img/facebook.png" width="30px"></a>`;
    if (student.youtube_student)
        socialMedia_div.innerHTML += `<a href="${student.youtube_student}"><img src="img/youtube.png" width="30px"></a>`;
    document.getElementById('studies').innerHTML = (student.studies_student+'').replace(/\r\n|\n\r|\r|\n/g,'<br>'); //gestion à la ligne

    // Récupération de l'avatar et affichage
    fetch(`../routeur.php/avatar/${student.id_student}`, {method: 'GET'})
    .then( response => response.json() )
    .then( data => {
        displayAvatarInHTML(data);
        fillAvatar(data);
    })
    .catch(error => {console.log(error) });

    //récupération des entreprises et affichage
    fetch(`../routeur.php/companies-student/${student.id_student}`, {method: 'GET'})
    .then( response => response.json())
    .then( data => {
        console.log(data);
        displayCompaniesInHTML(data);
    })
    .catch(error => {console.log(error) });
}

// Modifier un étudiant
document.getElementById('update').onclick = event => {
    event.preventDefault();
    document.location.href = "./update_student.html";
}

// Supprimer un étudiant
document.getElementById('delete').onclick = event => {
    event.preventDefault();

    fetch(`../routeur.php/student/${id_student}`, { method: 'DELETE'})
    .catch(error => { console.log(error) });

    document.location.href = "./students.html";
}

// Ajouter une expérience
document.getElementById('add-contract').onclick = event => {
    event.preventDefault();
    document.location.href = "./create_contract.html";
}

/**************************AVATAR**************************/

//Affichage de l'avatar 
function displayAvatarInHTML(avatar) {
    const list = document.getElementById("avatar-student");
    var content = 
        `<img src="img/face.png" style="width:100%">
        <img src="img/${avatar.hair_avatar}" style="width:100%">
        <img src="img/${avatar.eyebrows_avatar}" style="width:100%">
        <img src="img/${avatar.eyes_avatar}" style="width:100%">
        <img src="img/${avatar.mouth_avatar}" style="width:100%">`;
    
    if (avatar.glasses_avatar == 1)
        content += `<img src="img/glasses.png" style="width:100%">`;
    
    list.innerHTML = content;
}

function displayCompaniesInHTML(companies) {
    const list = document.getElementById("companies-student");

    var content = ``;

    companies.forEach(function (company){
        content += `<li class="company">${company.type_contract} ${company.study_level_contract} en ${company.field_contract} chez ${company.name_company}(${company.city_contract}) - ${company.field_company}</div><button id="delete-contract" onclick='removeContract(${company.id_contract})'>X</button>`;
    }) 

    list.innerHTML = content;
}

function removeContract(contract) {
    
    fetch(`../routeur.php/contract/${contract}`, { method: 'DELETE'})
    .catch(error => { console.log(error) });
    console.log(contract);
    document.location.href = "./student.html";
}

//SLIDE DES CHOIX POUR L'AVATAR

let slideIndex = [1,1,1,1]; //tableau qui gère à quel image on est pour chaque catégorie
const lengthSlide = [4, 3, 5, 6];
const slideId = ["hair", "eyebrows",    "eyes", "mouth"]; //tableau des noms des catégories
const hair = ["long_hair.png", "medium_hair.png", "short_hair.png", "bald_hair.png"];
const eyebrows = ["bushy_eyebrows.png", "thin_eyebrows.png", "unhappy_eyebrows.png"];
const eyes = ["round_eyes.png", "almond_shaped_eyes.png", "bulging_eyes.png", "closed_eyes.png", "makeup_eyes.png"];
const mouth = ["smiling_mouth.png", "unsmiling_mouth.png", "malicious_mouth.png", "beard_mouth.png", "mustache_mouth.png", "beard_mustache_mouth.png"];

function plusSlides(n, no) {
  showSlides(slideIndex[no] += n, no);
}

// n -> numéro de l'image
// no -> indice de la catégorie
function showSlides(n, no) {
    var length = lengthSlide[no];

    if (n > length)
        slideIndex[no] = 1;   

    if (n < 1) 
        slideIndex[no] = length;

    showAvatarDuringCreation();
}

function showAvatarDuringCreation() {
    document.getElementById('hair').setAttribute("src", "img/"+hair[slideIndex[0]-1]);
    document.getElementById('eyebrows').setAttribute("src", "img/"+eyebrows[slideIndex[1]-1]);
    document.getElementById('eyes').setAttribute("src", "img/"+eyes[slideIndex[2]-1]);
    document.getElementById('mouth').setAttribute("src", "img/"+mouth[slideIndex[3]-1]);
}

document.getElementById('select-glasses').onclick = event => {
    if(document.getElementById('select-glasses').checked)
        document.getElementById('glasses').style.display = "block";
    else
        document.getElementById('glasses').style.display = "none";
}

// Modifier l'avatar
document.getElementById('avatar-student').onclick = event => {
    event.preventDefault();

    //affichage de l'avatar modifiable
    document.getElementById('update-avatar').style.display = "block";
    document.getElementById('avatar-student').style.display = "none";

    document.getElementById('send_update_avatar').onclick = event => {
        event.preventDefault();

        const form = {};
        form.hair = hair[slideIndex[0]-1];
        form.eyebrows = eyebrows[slideIndex[1]-1];
        form.eyes = eyes[slideIndex[2]-1];
        form.mouth = mouth[slideIndex[3]-1];

        //vérification si la case lunette est coché ou non
        if(document.getElementById('select-glasses').checked)
            form.glasses = 1;
        else
            form.glasses = 0;

        fetch(`../routeur.php/avatar/${id_student}`, { method: 'PUT', body: JSON.stringify(form)})
        .catch(error => { console.log(error) });
        document.location.reload();
    }
}

// Préremplir l'avatar pour la modification
function fillAvatar(data)
{
    while (hair[slideIndex[0]-1] != data.hair_avatar)
        slideIndex[0]++;
    while (eyebrows[slideIndex[1]-1] != data.eyebrows_avatar)
        slideIndex[1]++;
    while (eyes[slideIndex[2]-1] != data.eyes_avatar)
        slideIndex[2]++;
    while (mouth[slideIndex[3]-1] != data.mouth_avatar)
        slideIndex[3]++;

    for ($i=0; $i<4; $i++)
        showSlides(slideIndex[$i],$i);

    if (data.glasses_avatar == 1)
    {
        document.getElementById('select-glasses').checked = true;
        document.getElementById('glasses').style.display = "block";
    }
}

