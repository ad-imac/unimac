/**************************STUDENT**************************/

// Créer un élève
function create_student(id_avatar){
  
    const form = {};
    form.firstname = document.getElementById("firstname-form").value;
    form.lastname = document.getElementById("lastname-form").value;
    form.mail = document.getElementById("mail-form").value;
    form.city = document.getElementById("city-form").value;
    form.year = document.getElementById("year-form").value;
    form.field = document.getElementById("field-form").value;
    form.portfolio = document.getElementById("portfolio-form").value;
    form.linkedin = document.getElementById("linkedin-form").value;
    form.instagram = document.getElementById("instagram-form").value;
    form.facebook = document.getElementById("facebook-form").value;
    form.youtube = document.getElementById("youtube-form").value;
    form.formation = document.getElementById("formation-form").value;
    form.id_avatar = id_avatar;
    fetch('../routeur.php/student', { method: 'POST', body: JSON.stringify(form)})
    .catch(error => { console.log(error) });
    document.location.href="./students.html";	
}

/**************************AVATAR**************************/

//SLIDE DES CHOIX POUR L'AVATAR

let slideIndex = [1,1,1,1]; //tableau qui gère à quel image on est pour chaque catégorie
const lengthSlide = [4, 3, 5, 6];
const slideId = ["hair", "eyebrows",    "eyes", "mouth"]; //tableau des noms des catégories
const hair = ["long_hair.png", "medium_hair.png", "short_hair.png", "bald_hair.png"];
const eyebrows = ["bushy_eyebrows.png", "thin_eyebrows.png", "unhappy_eyebrows.png"];
const eyes = ["round_eyes.png", "almond_shaped_eyes.png", "bulging_eyes.png", "closed_eyes.png", "makeup_eyes.png"];
const mouth = ["smiling_mouth.png", "unsmiling_mouth.png", "malicious_mouth.png", "beard_mouth.png", "mustache_mouth.png", "beard_mustache_mouth.png"];
showSlides(1, 0);
showSlides(1, 1);
showSlides(1, 2);
showSlides(1, 3);

function plusSlides(n, no) {
  showSlides(slideIndex[no] += n, no);
}

// n -> numéro de l'image
// no -> indice de la catégorie
function showSlides(n, no) {
    var length = lengthSlide[no];

    if (n > length)
        slideIndex[no] = 1;   

    if (n < 1) 
        slideIndex[no] = length;

    showAvatarDuringCreation();
}

function showAvatarDuringCreation() {
    document.getElementById('hair').setAttribute("src", "img/"+hair[slideIndex[0]-1]);
    document.getElementById('eyebrows').setAttribute("src", "img/"+eyebrows[slideIndex[1]-1]);
    document.getElementById('eyes').setAttribute("src", "img/"+eyes[slideIndex[2]-1]);
    document.getElementById('mouth').setAttribute("src", "img/"+mouth[slideIndex[3]-1]);
}

document.getElementById('select-glasses').onclick = event => {
    if(document.getElementById('select-glasses').checked)
        document.getElementById('glasses').style.display = "block";
    else
        document.getElementById('glasses').style.display = "none";
}



//FONCTIONS 

//création de l'avatar
document.getElementById('create_button').onclick = event => {
    event.preventDefault();

    var field = document.getElementById("field-form").value;
    if(field == "null"){
        console.log("remplissez la spécialité");
    }
    else{
        const form = {};
        form.hair = hair[slideIndex[0]-1];
        form.eyebrows = eyebrows[slideIndex[1]-1];
        form.eyes = eyes[slideIndex[2]-1];
        form.mouth = mouth[slideIndex[3]-1];

        //vérification si la case lunette est coché ou non
        if(document.getElementById('select-glasses').checked)
            form.glasses = 1;
        else
            form.glasses = 0;

        fetch('../routeur.php/avatar', { method: 'POST', body: JSON.stringify(form)})
        .then( response => response.json() )
        .then( data => {
            //data c'est l'id de l'avatar qui vient d'être créé
            //l'id en paramètre permet de remplir la clé étrangère
            //console.log(data);
            create_student(data);
        })
        .catch(error => { console.log(error) });	

    }


}
