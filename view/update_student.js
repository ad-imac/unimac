var id_student;

// récupération de l'identifiant de l'étudiant à partir de la session
fetch('../routeur.php/session', {method: 'GET'})
  .then( response => response.json() )
  .then ( data => {
        id_student = data;
        console.log(data);
        fetch(`../routeur.php/student/${id_student}`, { method: 'GET'})
        .then( response => response.json() )
        .then( data => {
            displayFilledForm(data);
        })
        .catch(error => {console.log(error) });
    })
  .catch(error => { console.log(error) });

/**************************STUDENT**************************/

// Modifier un étudiant
document.getElementById('update_student').onclick = event => {
    event.preventDefault();
    
    const form = {};
    form.firstname = document.getElementById("firstname-form").value;
    form.lastname = document.getElementById("lastname-form").value;
    form.mail = document.getElementById("mail-form").value;
    form.city = document.getElementById("city-form").value;
    form.year = document.getElementById("year-form").value;
    form.field = document.getElementById("field-form").value;
    form.portfolio = document.getElementById("portfolio-form").value;
    form.linkedin = document.getElementById("linkedin-form").value;
    form.instagram = document.getElementById("instagram-form").value;
    form.facebook = document.getElementById("facebook-form").value;
    form.youtube = document.getElementById("youtube-form").value;
    form.formation = document.getElementById("formation-form").value;
    
    fetch(`../routeur.php/student/${id_student}`, { method: 'PUT', body: JSON.stringify(form)})
    .catch(error => { console.log(error) });
    document.location.href = "./student.html";
} 
  
//préremplir le formulaire de modification
function displayFilledForm(data)
{
    document.getElementById("firstname-form").value = data.firstname_student;
    document.getElementById("lastname-form").value = data.lastname_student;
    document.getElementById("mail-form").value = data.mail_student;
    document.getElementById("city-form").value = data.city_student;
    document.getElementById("year-form").value = data.year_student;
    document.getElementById("field-form").value = data.field_student;
    document.getElementById("portfolio-form").value = data.portfolio_student;
    document.getElementById("linkedin-form").value = data.linkedin_student;
    document.getElementById("instagram-form").value = data.insta_student;
    document.getElementById("facebook-form").value = data.facebook_student;
    document.getElementById("youtube-form").value = data.youtube_student;
    document.getElementById("formation-form").value = data.studies_student;


    //pour préremplir la liste déroulante des fields
    var allInputs = document.getElementById("field-form").children;
    for(i=0; i<allInputs.length; i++){
        if(allInputs[i].innerHTML == data.field_student){
            allInputs[i].defaultSelected = true;
        }
    }
}