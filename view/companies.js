Document.prototype.ready = callback => {
    if (callback && typeof callback === 'function') {
        document.addEventListener("DOMContentLoaded", () => {
            if (document.readyState === "interactive" || document.readyState === "complete") {
                return callback();
            }
        });
    }
};

document.ready(() => {
    fetch('../routeur.php/companies-tri', {method: 'GET'})
    .then( response => response.json() )
    .then( data => {
        console.log(data);
        displayListeDeroulanteContractFields(data[0]);
        displayListeDeroulanteContractType(data[1]);
    })
    .catch(error => {console.log(error) });

    fetch("../routeur.php/companies", { method: 'GET' })
        .then(response => response.json())
        .then(data => {
            displayCompanies(data);
        })
        .catch(error => { console.log(error) });
});

function displayListeDeroulanteContractFields(contractFields){

    const list = document.getElementById('listContractField');
    var content = `<option value="tous">Trier par domaine</option>`;
    contractFields.forEach(function (contractField){
        content += `<option value="${contractField.field_contract}">${contractField.field_contract}</option>`; 
    })
    list.innerHTML = content;
}

function displayListeDeroulanteContractType(contractTypes){

    const list = document.getElementById('listContractType');
    var content = `<option value="tous">Trier par type de contrat</option>`;
    contractTypes.forEach(function (contractType){
        content += `<option value="${contractType.type_contract}">${contractType.type_contract}</option>`; 
    })
    list.innerHTML = content;
}

function displayCompanies(companies) {
    const list = document.getElementById('display_companies');
    var content = "";
    companies.forEach(function (company) {
        content += "<div class='carte-companies'><p>" + company.name_company + "</p><p>" + company.field_company + "</p></div>";
    });
    list.innerHTML = content;
}

//CHANGEMENT DE L'AFFICHAGE DES ETUDIANTS EN FONCTION DE LA BARRE DE TRI
document.getElementById('submit_search_companies').onclick = event => {
    event.preventDefault();

    const search = {};
    search.contractField = document.getElementById("listContractField").value;
    search.contractType = document.getElementById("listContractType").value;

    console.log(search);

    fetch(`../routeur.php/companies-filtered/${search.contractField}/${search.contractType}`, {method: 'GET'})
    .then( response => response.json() )
    .then( data => {
        console.log(data);
        displayCompanies(data);
    })
    .catch(error => {console.log(error) });
}