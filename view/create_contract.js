var id_student;

//par défaut, on dit qu'on ne crée pa d'entreprise
var aCompanyIsCreated = false;

// récupération de l'identifiant de l'étudiant à partir de la session
fetch('../routeur.php/session', {method: 'GET'})
  .then( response => response.json() )
  .then ( data => {
      id_student = data;

      console.log(id_student);
    })    
.catch(error => { console.log(error) });

Document.prototype.ready = callback => {
	if(callback && typeof callback === 'function') {
		document.addEventListener("DOMContentLoaded", () =>  {
			if(document.readyState === "interactive" || document.readyState === "complete") {
				return callback();
			}
		});
	}
};

//Afficher la liste des entreprises
document.ready( () => {
  //aller chercher les infos pour la liste déroulante des entreprises existantes
  fetch('../routeur.php/companies', {method: 'GET'})
      .then( response => response.json() )
  .then( data => {
          console.log(data);
          displayListeDeroulanteCompanies(data);
  })
  .catch(error => {console.log(error) });
});

function displayListeDeroulanteCompanies(companies){

  const list = document.getElementById('listeCompanies');
  var content = `<option value="tous">Entreprises existantes</option>`;
  companies.forEach(function (company){
      content += `<option value="${company.id_company}">${company.name_company}</option>`; 
  })
  list.innerHTML = content;
}




//afficher formulaire de création d'entreprise si elle n'existe pas encore dans la base de données
document.getElementById('create-company-button').onclick = event => {
  event.preventDefault();

  const list = document.getElementById('create-company-form');
  var content = `<input id = "name-company-form"
  type = "text"
  maxlength = "50"
  placeholder = "Nom de l'entreprise"
  />

  <select id = "field-company-form">
      <option value = "null">Domaine</option>
      <option value = "agriculture">Agriculture</option>
      <option value = "industries extractives">Industries extractives</option>
      <option value = "industries manufacturières">Industries manufacturières</option>
      <option value = "energie">Energie</option>
      <option value = "eau-déchets">Eau-déchets</option>
      <option value = "btp">BTP</option>
      <option value = "commerce">Commerce</option>
      <option value = "transport">Transport</option>
      <option value = "hébergement-restauration">Hébergement-Restauration</option>
      <option value = "information-communication">Information-Communication</option>
      <option value = "finance-assurance">Finance-Assurance</option>
      <option value = "immobilier">Immobilier</option>
      <option value = "sciences-technique">Sciences-Technique</option>
      <option value = "services administratifs">Services administratifs</option>
      <option value = "administration publique">Administration publique</option>
      <option value = "enseignement">Enseignement</option>
      <option value = "santé-social">Santé-Social</option>
      <option value = "arts-spectacles-loisirs">Arts-Spectacles-Loisirs</option>
      <option value = "services autres">Services autres</option>
      <option value = "activités extra-territoriales">Activités extra-territoriales</option>
  </select>`;
  list.innerHTML = content;

  document.getElementById('listeCompanies').style.display = "none";
  document.getElementById('create-company-button').style.display = "none";

  aCompanyIsCreated = true;
}



/**************************CONTRACT**************************/
// Créer une expérience
document.getElementById('create_contract').onclick = event => {
  event.preventDefault();

  var id_company;

  //si une entreprise doit être créée
  if(aCompanyIsCreated == true){
      console.log(document.getElementById("field-company-form").value);
      
      const form = {};
      form.name_company = document.getElementById("name-company-form").value;
      form.field_company = document.getElementById("field-company-form").value;

      console.log(form);

      fetch('../routeur.php/company', { method: 'POST', body: JSON.stringify(form)})
      .then( response => response.json())
      .then( data => {
          //data c'est l'id de l'entreprise qui vient d'être créée
          create_contract(data);
      })
  }
  else{
    id_company = document.getElementById("listeCompanies").value;
    create_contract(id_company);
  }
  
}

//créer un contrat
function create_contract(id_company){
  
  const form = {};
  form.type_contract = document.getElementById("type-contract-form").value;
  form.field_contract = document.getElementById("field-contract-form").value;
  form.city_contract = document.getElementById("city-contract-form").value;
  form.study_level_contract = document.getElementById("study-level-contract-form").value;
  form.id_student = id_student;
  form.id_company= id_company;

  fetch('../routeur.php/contract', { method: 'POST', body: JSON.stringify(form)})
  .catch(error => { console.log(error) });
  document.location.href="./student.html";
}