Document.prototype.ready = callback => {
	if(callback && typeof callback === 'function') {
		document.addEventListener("DOMContentLoaded", () =>  {
			if(document.readyState === "interactive" || document.readyState === "complete") {
				return callback();
			}
		});
	}
};

//Afficher les élèves
document.ready( () => {

    //aller chercher les infos pour la barre de tri
    fetch('../routeur.php/students-tri', {method: 'GET'})
        .then( response => response.json() )
		.then( data => {
            console.log(data);
            displayListeDeroulanteField(data[0]);
            displayListeDeroulanteYear(data[1]);
		})
    .catch(error => {console.log(error) });

    fetch('../routeur.php/students', {method: 'GET'})
        .then( response => response.json() )
		.then( data => {
            //console.log(data);
			displayStudentsInHTML(data);
		})
    .catch(error => {console.log(error) });

});

function displayListeDeroulanteField(fields){

    const list = document.getElementById('listeField');
    var content = `<option value="tous">Trier par spécialité</option>`;
    fields.forEach(function (field){
        content += `<option value="${field.field_student}">${field.field_student}</option>`; 
    })
    list.innerHTML = content;
}

function displayListeDeroulanteYear(year){

    const list = document.getElementById('listeYear');
    var content = `<option value="tous">Trier par promotion</option>`;
    year.forEach(function (year){
        content += `<option value="${year.year_student}">${year.year_student}</option>`; 
    })
    list.innerHTML = content;
}

//Affichage des informations des étudiants
function displayStudentsInHTML(students) {
    const list = document.getElementById('display_students');
    
    var content = ``;
    students.forEach(function (student){
        content += `<div class="carte" id ="${student.id_student}"><div class="avatar-affiche-students" id="${student.id_avatar}-avatars"></div><p class="text1">${student.firstname_student}</p><p class="text2">${student.lastname_student}</p><p class="text3">${student.field_student}</p></div>`;
        
        //récupération de l'avatar et affichage
        fetch(`../routeur.php/avatar/${student.id_student}`, {method: 'GET'})
        .then( response => response.json() )
		.then( data => {
			displayAvatarInHTML(data);
		})
        .catch(error => {console.log(error) });
    })

    list.innerHTML = content;

    //Création des liens sur chaque étudiant en récupérant l'id
    var students_carte = document.getElementsByClassName("carte");
    for (let student_carte of students_carte) 
    {
        student_carte.onclick = event => {
            event.preventDefault();

            //On set dans la session l'id de l'étudiant selectioné
            fetch(`../routeur.php/session/${student_carte.id}`, {method: 'PUT'})
            .catch(error => { console.log(error) });

            document.location.href = "./student.html";
        }
    }
}

//Affichage de l'avatar 
function displayAvatarInHTML(avatar) {
    const list = document.getElementById(`${avatar.id_avatar}-avatars`);
    var content = 
        `<img src="img/face.png" style="width:100%">
        <img src="img/${avatar.hair_avatar}" style="width:100%">
        <img src="img/${avatar.eyebrows_avatar}" style="width:100%">
        <img src="img/${avatar.eyes_avatar}" style="width:100%">
        <img src="img/${avatar.mouth_avatar}" style="width:100%">`;
    
    if (avatar.glasses_avatar == 1)
        content += `<img src="img/glasses.png" style="width:100%">`;
    
    list.innerHTML = content;
}

//CHANGEMENT DE L'AFFICHAGE DES ETUDIANTS EN FONCTION DE LA BARRE DE TRI
document.getElementById('submit_search_students').onclick = event => {
    event.preventDefault();

    const search = {};
    search.field = document.getElementById("listeField").value;
    search.year = document.getElementById("listeYear").value;

    console.log(search);

    fetch(`../routeur.php/students-filtered/${search.field}/${search.year}`, {method: 'GET'})
    .then( response => response.json() )
    .then( data => {
        console.log(data);
        displayStudentsInHTML(data);
    })
    .catch(error => {console.log(error) });
}