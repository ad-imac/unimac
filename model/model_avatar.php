<?php

    require_once('connection.php');

    function get_avatar($id_student)
    {
        $cnx = connection();
		$rqt = $cnx->prepare('SELECT avatar.* FROM avatar JOIN student ON student.id_avatar=avatar.id_avatar WHERE student.id_student = ?');
		$rqt->execute(array($id_student));
        $avatar = $rqt->fetch(PDO::FETCH_ASSOC);
    
    	return $avatar;
    }

    function create_avatar($hair, $eyes, $eyebrows, $mouth, $glasses)
    {
        $cnx = connection();
		$rqt = $cnx->prepare('INSERT INTO avatar(hair_avatar, eyes_avatar, eyebrows_avatar, mouth_avatar, glasses_avatar) VALUES( ?, ?, ?, ?, ?)');
		$rqt->execute(array($hair, $eyes, $eyebrows, $mouth, $glasses));
        return $cnx->lastInsertId(); //on récupère l'id de l'élément qui vient d'être créé
        echo $cnx->lastInsertId();
    }

    function update_avatar($hair, $eyes, $eyebrows, $mouth, $glasses, $id)
    {
        $cnx = connection();
		$rqt = $cnx->prepare('UPDATE avatar SET hair_avatar = ?, eyes_avatar = ?, eyebrows_avatar = ?, mouth_avatar = ?, glasses_avatar = ? WHERE id_avatar = ?');
		$rqt->execute(array($hair, $eyes, $eyebrows, $mouth, $glasses, $id));
    }

?>