<?php
require_once('connection.php');
    function create_student($firstname, $lastname, $mail, $city, $year, $field, $portfolio, $linkedin, $instagram, $facebook, $youtube, $formation, $id_avatar)
    {
        $cnx = connection();
        $rqt = $cnx->prepare('INSERT INTO student(firstname_student, lastname_student, mail_student, city_student, year_student, field_student, portfolio_student, linkedin_student, insta_student, facebook_student, youtube_student, studies_student, id_avatar) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
		$rqt->execute(array($firstname, $lastname, $mail, $city, $year, $field, $portfolio, $linkedin, $instagram, $facebook, $youtube, $formation, $id_avatar));
    }

    function update_student($firstname, $lastname, $mail, $city, $year, $field, $portfolio, $linkedin, $instagram, $facebook, $youtube, $formation, $id)
    {
        $cnx = connection();
		$rqt = $cnx->prepare('UPDATE student SET firstname_student = ?, lastname_student = ?, mail_student = ?, city_student = ?, year_student = ?, field_student = ?, portfolio_student = ?, linkedin_student = ?, insta_student = ?, facebook_student = ?, youtube_student = ?, studies_student = ? WHERE id_student = ?');
		$rqt->execute(array($firstname, $lastname, $mail, $city, $year, $field, $portfolio, $linkedin, $instagram, $facebook, $youtube, $formation, $id));
    }

    function get_all_students(){
        $cnx = connection();
        $rqt = 'SELECT * FROM student';
        $stmt = $cnx->query($rqt); 
        $result = $stmt->fetchall(PDO::FETCH_ASSOC);
        return $result;
    }

    function get_student($id){
        $cnx = connection();
        $rqt = "SELECT * FROM student WHERE id_student = {$id}";
        $stmt = $cnx->query($rqt);
        $student = $stmt->fetch();
        return $student;
    }

    function get_field(){
        $cnx = connection();
        $rqt = "SELECT DISTINCT field_student FROM student";
        $stmt = $cnx->query($rqt); 
        $result = $stmt->fetchall(PDO::FETCH_ASSOC);
        return $result;
    }

    function get_year(){
        $cnx = connection();
        $rqt = "SELECT DISTINCT year_student FROM student";
        $stmt = $cnx->query($rqt); 
        $result = $stmt->fetchall(PDO::FETCH_ASSOC);
        return $result;
    }

    function get_student_by_field_and_year($field, $year){
        $cnx = connection();
        //$field = $_GET['field_student'];
        // $year = $_GET['year_student'];

        if($field == "tous" && $year == "tous"){
            $resultat = 'SELECT * FROM student';
        }
        else if($field == "tous" && $year != "tous") {
            $resultat = 'SELECT * FROM student WHERE year_student = "'.$year.'"';
        }
        else if($field != "tous" && $year == "tous") {
            $resultat = 'SELECT * FROM student WHERE field_student = "'.$field.'"';
        }
        else{
            $resultat = 'SELECT * FROM student WHERE field_student = "'.$field.'" AND year_student = "'.$year.'"';
        }
        
        $stmt = $cnx->query($resultat); 
        $tri = $stmt->fetchall(PDO::FETCH_ASSOC);
        return $tri;
    }

    function delete_student($id)
    {
        $cnx = connection();
        $rqt1 = $cnx->prepare('DELETE FROM contract WHERE id_student = ?');
        $rqt2 = $cnx->prepare('DELETE student,avatar FROM student JOIN avatar ON avatar.id_avatar=student.id_avatar WHERE student.id_student = ?');
		$rqt1->execute(array($id));
        $rqt2->execute(array($id));
    }

?>