<?php

require_once('connection.php');

function create_contract($type_contract, $field_contract, $city_contract, $study_level_contract, $id_student, $id_company)
{
    $cnx = connection();
    $rqt = $cnx->prepare('INSERT INTO contract(type_contract, field_contract, city_contract, study_level_contract, id_student, id_company) VALUES( ?, ?, ?, ?, ?, ?)');
    $rqt->execute(array($type_contract, $field_contract, $city_contract, $study_level_contract, $id_student, $id_company));
}

function delete_contract($id)
{
    $cnx = connection();
    $rqt = $cnx->prepare("DELETE FROM contract WHERE id_contract = ?");
    $rqt->execute(array($id));
}

?>