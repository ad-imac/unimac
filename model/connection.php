<?php

    //en espérant que cette connexion fonctionne pour tout le monde
    function connection() {
		try {
			// Structure conditionelle vérifiant le système d'exploitation de la développeuse/eur 
			if (PHP_OS_FAMILY == "Windows") { // Pour Windows
				$cnx = new PDO('mysql:host=localhost;dbname=unimac','root','');
			} 
			else if (PHP_OS_FAMILY == "Darwin") { // Pour Mac
				$cnx = new PDO('mysql:host=localhost;dbname=unimac','root','root'); 
			}
			$cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (PDOException $e) {
			echo 'Échec de la connexion : ' . $e->getMessage();
		}
		return $cnx;
	}

?>

