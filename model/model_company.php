<?php
require_once('connection.php');

function get_all_companies(){
    $cnx = connection();
    $rqt = 'SELECT * FROM company';
    $stmt = $cnx->query($rqt); 
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $result;
}

function create_company($name_company, $field_company)
{
    $cnx = connection();
    $rqt = $cnx->prepare('INSERT INTO company(name_company,field_company) VALUES( ?, ?)');
    // echo mb_detect_encoding(utf8_decode($field_company));
    $rqt->execute(array($name_company, $field_company));
    // $rqt->execute(array($name_company, utf8_decode($field_company)));
    return $cnx->lastInsertId(); //on récupère l'id de l'élément qui vient d'être créé
    echo $cnx->lastInsertId();
}

function get_companies_by_student($id_student) {
    $cnx = connection();
	$rqt = $cnx->prepare("SELECT * FROM company JOIN contract ON company.id_company=contract.id_company WHERE contract.id_student = ?");
    $rqt->execute(array($id_student));
    $company = $rqt->fetchall(PDO::FETCH_ASSOC);
    return $company;
}

function get_name_companies() {
    $cnx = connection();
    $rqt = "SELECT DISTINCT name_company FROM company";
    $stmt = $cnx->query($rqt); 
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $result;
}

function get_contractField(){
    $cnx = connection();
    $rqt = "SELECT DISTINCT field_contract FROM contract JOIN company ON contract.id_company=company.id_company";
    $stmt = $cnx->query($rqt); 
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $result;
}

function get_contractType(){
    $cnx = connection();
    $rqt = "SELECT DISTINCT type_contract FROM contract JOIN company ON contract.id_company=company.id_company";
    $stmt = $cnx->query($rqt); 
    $result = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $result;
}

function get_by_field_and_type($fieldContract, $typeContract){
    $cnx = connection();
    // $fieldContract = $_GET['field_contract'];
    // $typeContract = $_GET['type_contract'];

    if($fieldContract == "tous" && $typeContract == "tous"){
        $resultat = 'SELECT * FROM company';
    }
    else if($fieldContract != "tous" && $typeContract == "tous") {
        $resultat = 'SELECT * FROM company JOIN contract ON company.id_company=contract.id_company WHERE field_contract = "'.$fieldContract.'"';
    }
    else if($fieldContract == "tous" && $typeContract != "tous") {
        $resultat = 'SELECT * FROM company JOIN contract ON company.id_company=contract.id_company WHERE type_contract = "'.$typeContract.'"';
    }
    else{
        $resultat = 'SELECT * FROM company JOIN contract ON company.id_company=contract.id_company WHERE field_contract = "'.$fieldContract.'" AND type_contract = "'.$typeContract.'"';
    }
    
    $stmt = $cnx->query($resultat); 
    $tri = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $tri;
}