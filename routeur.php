<?php

require_once('controller/ctrl_avatar.php');
require_once('controller/ctrl_student.php');
require_once('controller/ctrl_company.php');
require_once('controller/ctrl_contract.php');

$page = explode('/', $_SERVER['REQUEST_URI']);
$method = $_SERVER['REQUEST_METHOD'];

//parcours de l'url pour trouver routeur.php
for ($i = 0; $i < count($page); $i++) {
	if ($page[$i] == "routeur.php") {
		$endpoint = $i + 1;
		break;
	}
}

// Pour nous Laura : au lieu de swith($page[2]), nous c'est switch($page[count($page)-1])
switch ($page[$endpoint]) {
	case 'avatar':

		switch ($method) {
			case 'GET':
				echo display_avatar($page[$endpoint + 1]);
				break;
			case 'POST':
				$json = file_get_contents('php://input');
				echo add_avatar($json);
				break;
			case 'PUT':
				$json = file_get_contents('php://input');
				echo modify_avatar($json, $page[$endpoint + 1]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'student':
		switch ($method) {
			case 'GET':
				echo get_a_student($page[$endpoint + 1]);
				break;
			case 'POST':
				$json = file_get_contents('php://input');
				echo add_student($json);

				break;
			case 'PUT':
				$json = file_get_contents('php://input');
				echo modify_student($json, $page[$endpoint + 1]);
				break;
			case 'DELETE':
				$json = file_get_contents('php://input');
				echo delete_a_student($page[$endpoint + 1]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'students':
		switch ($method) {
			case 'GET':
				echo get_students();
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'students-tri':
		switch ($method) {
			case 'GET':
				echo json_encode(array(get_by_field_student(), get_by_year_student()));
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'students-filtered':
		switch ($method) {
			case 'GET':
				echo get_by_field_and_year_student($page[$endpoint + 1], $page[$endpoint + 2]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'companies-student':
		switch ($method) {
			case 'GET':
				echo get_by_student_companies($page[$endpoint + 1]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'session':
		switch ($method) {
			case 'GET':
				echo get_session_student_id();
				break;
			case 'PUT':
				echo set_session_student_id($page[$endpoint + 1]);
				break;
		}
		break;

	case 'companies':
		switch ($method) {
			case 'GET':
				echo get_companies();
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'companies-tri':
		switch ($method) {
			case 'GET':
				echo json_encode(array(get_by_field_contract(), get_by_type_contract()));
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'companies-filtered':
		switch ($method) {
			case 'GET':
				echo get_by_field_and_type_contract($page[$endpoint + 1], $page[$endpoint + 2]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
			}
			break;

	case 'contract':
		switch ($method) {
			case 'POST':
				$json = file_get_contents('php://input');
				echo add_contract($json);
				break;
			case 'DELETE':
				echo delete_a_contract($page[$endpoint + 1]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;
	
	case 'company':
		switch ($method) {
			case 'POST':
				$json = file_get_contents('php://input');
				echo add_company($json);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	default:
		http_response_code('500');
		echo 'unknown endpoint';
		break;
}
