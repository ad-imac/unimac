<?php

    require_once('model/model_student.php');
    session_start();

    function add_student($form) {
        $student = json_decode($form, true);
        return json_encode(create_student($student['firstname'], $student['lastname'], $student['mail'], $student['city'], $student['year'], $student['field'], $student['portfolio'], $student['linkedin'], $student['instagram'], $student['facebook'], $student['youtube'], $student['formation'], $student['id_avatar']));
    }

    function modify_student($form, $id) {
        $student = json_decode($form, true);
        return json_encode(update_student($student['firstname'], $student['lastname'], $student['mail'], $student['city'], $student['year'], $student['field'], $student['portfolio'], $student['linkedin'], $student['instagram'], $student['facebook'], $student['youtube'], $student['formation'], $id));
    }

    function get_students(){
        $rslt = get_all_students();
        return json_encode($rslt);
    }

    function get_a_student($id){
        //$student = get_student($id);
        //return $student;
        return json_encode(get_student($id));

    }

    function get_by_field_student() {
        $studentfield = get_field();
        return $studentfield;
    }

    function get_by_year_student() {
        $studentyear = get_year();
        return $studentyear;
    }

    function get_by_field_and_year_student($field, $year){
        $studentfilter = get_student_by_field_and_year($field, $year);
        return json_encode($studentfilter);
    }

    function delete_a_student($id) {
		return json_encode(delete_student($id));
	}

    function set_session_student_id($id) {
        $_SESSION["id"] = $id;
    }
    function get_session_student_id() {
        return json_encode($_SESSION["id"]);
    }
?>