<?php

    require_once('model/model_avatar.php');

    function display_avatar($id_student) {
        return json_encode(get_avatar($id_student));
    }

    function modify_avatar($form, $id) {
        $avatar = json_decode($form, true);
        return json_encode(update_avatar($avatar['hair'], $avatar['eyes'], $avatar['eyebrows'], $avatar['mouth'], $avatar['glasses'], $id));
    }

    function add_avatar($form) {
        $avatar = json_decode($form, true);
        return json_encode(create_avatar($avatar['hair'], $avatar['eyes'], $avatar['eyebrows'], $avatar['mouth'], $avatar['glasses']));
    }

?>