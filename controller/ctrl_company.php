<?php
    require_once('model/model_company.php');

    function get_companies(){
        $rslt = get_all_companies();
        // echo mb_detect_encoding(json_encode($rslt));

        // pour régler les problèmes d'accent
        // $i = 0;
        // foreach($rslt as $ligne){
        //     $rslt[$i]["field_company"] = mb_convert_encoding ($ligne["field_company"] , "UTF-8" , "ASCII" );
        //     $i++;
        // }
        
        // return json_encode($rslt, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        return json_encode($rslt);

    }

    function add_company($form){
        $company = json_decode($form, true);
        return json_encode(create_company($company['name_company'], $company['field_company']));
    }

    function get_by_student_companies($id_student) {
        $rslt = get_companies_by_student($id_student);
        
        // pour régler les problèmes d'accent
        // $i = 0;
        // foreach($rslt as $ligne){
        //     $rslt[$i]["field_company"] = mb_convert_encoding ($ligne["field_company"] , "UTF-8" , "ASCII" );
        //     $i++;
        // }
        return json_encode($rslt);
        // return json_encode($rslt, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

    }

    function get_companies_name() {
        return json_encode(get_name_companies());
        // return json_encode(get_name_companies(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

    }

    function get_by_field_contract() {
        $contractfield = get_contractField();
        return $contractfield;
    }

    function get_by_type_contract() {
        $contracttype = get_contractType();
        return $contracttype;
    }

    function get_by_field_and_type_contract($fieldContract, $fieldType){
        $contractfilter = get_by_field_and_type($fieldContract, $fieldType);
        // pour régler les problèmes d'accent
        // $i = 0;
        // foreach($contractfilter as $ligne){
        //     $contractfilter[$i]["field_company"] = mb_convert_encoding ($ligne["field_company"] , "UTF-8" , "ASCII" );
        //     $i++;
        // }
        return json_encode($contractfilter);
        // return json_encode($contractfilter, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

?>