<?php
    require_once('model/model_contract.php');

    function add_contract($form) {
        $contract = json_decode($form, true);
        return json_encode(create_contract($contract['type_contract'], $contract['field_contract'], $contract['city_contract'], $contract['study_level_contract'], $contract['id_student'], $contract['id_company']));
    }

    function delete_a_contract($id) {
		return json_encode(delete_contract($id));
    }

?>